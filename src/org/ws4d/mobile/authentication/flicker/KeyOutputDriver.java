package org.ws4d.mobile.authentication.flicker;

public interface KeyOutputDriver {
	Boolean driveKeyOut(Boolean[] key);
	void setSymbolWidth(int ms);
}
