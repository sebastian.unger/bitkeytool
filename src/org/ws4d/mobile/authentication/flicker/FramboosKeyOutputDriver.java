package org.ws4d.mobile.authentication.flicker;

import framboos.OutPin;

public class FramboosKeyOutputDriver implements KeyOutputDriver {

	private OutPin mOutputPin = null;
	private int mSymbolWidth = 500;

	public FramboosKeyOutputDriver() {
		
	}
	
	public FramboosKeyOutputDriver(int pin) {
		setOutputPin(pin);
	}
	
	public FramboosKeyOutputDriver(OutPin pin) {
		this.mOutputPin = pin;
	}
	
	public void setOutputPin(int pin) {
		this.mOutputPin = new OutPin(pin);
	}
	
	@Override
	public Boolean driveKeyOut(Boolean[] key) {
		if (mOutputPin == null)
			return false;

		/* set low */
		mOutputPin.setValue(false);
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		/* key */
		for (int i = 0; i < key.length; i++) {
			mOutputPin.setValue(key[i]);
			try {
				Thread.sleep(mSymbolWidth);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}		

		/* set low */
		mOutputPin.setValue(false);
		
		return true;
	}

	@Override
	public void setSymbolWidth(int ms) {
		this.mSymbolWidth = ms;		
	}

}
